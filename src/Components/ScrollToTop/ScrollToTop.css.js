export const useStyles = (theme) => ({
  scrollTopStyle: {
    padding: 20,
    backgroundColor: "#eee",
    width: 15,
    height: 15,
    borderRadius: "50%",
    position: "fixed",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    left: "92%",
    bottom: 70,
    "&:hover": {
      cursor: "pointer",
      backgroundColor: "#181818",
      color: "#fff",
    },
    [theme.breakpoints.down("md")]: {
      left: "85%",
      bottom: 50,
    },
  },
});
