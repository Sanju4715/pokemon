import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tooltip from "@material-ui/core/Tooltip";

import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";

import { useStyles } from "./ScrollToTop.css";

class ScrollToTop extends Component {
  constructor(props) {
    super(props);
    this.state = { scrollTop: 0 };
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    const pageOffSet =
      document.documentElement.scrollTop || document.body.scrollTop;
    this.setState({
      scrollTop: pageOffSet,
    });
  };

  handleScrollTop = () => {
    window.scroll(0, 0);
  };

  render() {
    const { classes } = this.props;
    return (
      this.state.scrollTop > 500 && (
        <Tooltip arrow title="Scroll To Top">
          <Paper
            elevation={5}
            className={classes.scrollTopStyle}
            onClick={this.handleScrollTop}
          >
            <ArrowUpwardIcon style={{ width: 35, height: 35 }} />
          </Paper>
        </Tooltip>
      )
    );
  }
}

export default withStyles(useStyles)(ScrollToTop);
