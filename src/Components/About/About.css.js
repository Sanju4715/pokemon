export const useStyles = (theme) => ({
  root: {
    display: "flex",
    backgroundColor: "#eee",
    minHeight: "90vh",
    padding: 20,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
});
