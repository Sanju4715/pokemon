import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";

import { useStyles } from "./About.css";
import Header from "../Header/Header";

class About extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Header />
        <div className={classes.content}>
          <div className={classes.toolbar} />
          <div>API used https://pokeapi.co/</div>
          <div>Developed By Sanjaya Rai</div>
          <a
            href="https://sanjayarai.netlify.app"
            target="_blank"
            rel="noreferrer"
          >
            Go To Portfolio
          </a>
          <div>{new Date().getFullYear()} &copy; All rights Reserved</div>
        </div>
      </div>
    );
  }
}

export default withStyles(useStyles)(About);
