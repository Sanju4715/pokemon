import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Skeleton from "@material-ui/lab/Skeleton";
import { connect } from "react-redux";

import { useStyles } from "./PokemonCard.css";
import { renderColor } from "../../RenderColor/renderColor";
import axios from "axios";
import { updatePokemons } from "../../Redux/Action/Action";

class PokemonCard extends Component {
  constructor(props) {
    super(props);
    this.state = { pokemonDetails: {} };
  }

  componentDidMount() {
    this.getPokemonDetails(this.props.pokemon);
  }

  getPokemonDetails = async (pokemon) => {
    try {
      let tempArr = pokemon.url.split("/");
      let pokemonId = tempArr[tempArr.length - 2];
      let dataExist = this.props.pokemons.find((data) => data.id === pokemonId);
      if (dataExist) {
        this.setState({ pokemonDetails: dataExist });
      } else {
        const responseDetails = await axios.get(
          `https://pokeapi.co/api/v2/pokemon/${pokemonId}/`
        );
        const speciesDetails = await axios.get(
          responseDetails.data.species.url
        );
        let tempTypes = responseDetails.data.types.map(
          (type) => type.type.name
        );
        let tempData = {
          id: pokemonId,
          name: pokemon.name,
          url: pokemon.url,
          image: responseDetails.data.sprites.other.dream_world.front_default,
          types: tempTypes,
          habitat: speciesDetails.data.habitat.name,
        };
        this.props.updatePokemons(tempData);
        this.setState({ pokemonDetails: tempData });
      }
    } catch (error) {
      console.log(error);
    }
  };

  sendDetails = (id) => {
    this.props.history.push(`/pokemon/${id}`);
  };

  render() {
    const { classes } = this.props;
    const { pokemonDetails } = this.state;
    return (
      <Grid item xs={6} sm={6} md={3} xl={2} lg={2}>
        {pokemonDetails.id ? (
          <Paper
            className={classes.paper}
            onClick={() => this.sendDetails(pokemonDetails.id)}
            style={{
              background:
                pokemonDetails.types.length < 2
                  ? renderColor(pokemonDetails.types[0])
                  : `linear-gradient(to right, ${renderColor(
                      pokemonDetails.types[0]
                    )},${renderColor(pokemonDetails.types[1])})`,
            }}
          >
            <img
              src={pokemonDetails.image}
              alt={pokemonDetails.name}
              style={{ width: 75, height: 75 }}
            />
            <div>
              {pokemonDetails.name.charAt(0).toUpperCase() +
                pokemonDetails.name.slice(1)}
            </div>
          </Paper>
        ) : (
          <Skeleton variant="rect" width={150} height={118} />
        )}
      </Grid>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pokemons: state.PokemonReducer.pokemons,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updatePokemons: (pokemons) => {
      dispatch(updatePokemons(pokemons));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(withRouter(PokemonCard)));
