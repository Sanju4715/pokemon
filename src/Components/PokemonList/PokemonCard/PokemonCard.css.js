export const useStyles = (theme) => ({
  paper: {
    padding: 10,
    color: "#fff",
    fontWeight: "bold",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    "&:hover": {
      cursor: "pointer",
      opacity: 0.7,
    },
  },
});
