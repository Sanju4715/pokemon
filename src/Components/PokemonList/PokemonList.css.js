export const useStyles = (theme) => ({
  root: {
    display: "flex",
    backgroundColor: "#eee",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  paper: {
    padding: 10,
    color: "#fff",
    fontWeight: "bold",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    "&:hover": {
      cursor: "pointer",
      opacity: 0.7,
    },
  },
  inputRoot: {
    height: 70,
  },
  labelRoot: {
    fontSize: 18,
    fontWeight: "bold",
  },
  notchedOutline: {},
  labelFocused: { color: "black !important" },
  focused: {
    "& $notchedOutline": {
      borderColor: "black !important",
    },
  },
  linkStyle: {
    "&:hover": {
      cursor: "pointer",
    },
  },
  scrollTopStyle: {
    padding: 20,
    backgroundColor: "#eee",
    width: 15,
    height: 15,
    borderRadius: "50%",
    position: "fixed",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    left: "92%",
    bottom: 70,
    "&:hover": {
      cursor: "pointer",
      backgroundColor: "#181818",
      color: "#fff",
    },
    [theme.breakpoints.down("md")]: {
      left: "85%",
      bottom: 50,
    },
  },
});
