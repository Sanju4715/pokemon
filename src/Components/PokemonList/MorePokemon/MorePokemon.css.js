export const useStyles = (theme) => ({
  linkStyle: {
    padding: 15,
    width: 15,
    height: 15,
    borderRadius: "50%",
    backgroundColor: "#181818",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#eee",
    "&:hover": {
      cursor: "pointer",
    },
  },
});
