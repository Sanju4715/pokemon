import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import Paper from "@material-ui/core/Paper";
import MoreRoundedIcon from "@material-ui/icons/MoreRounded";

import { useStyles } from "./MorePokemon.css";

class MorePokemon extends Component {
  render() {
    const { classes, nextUrl, handlePokemonLimitandOffset } = this.props;
    return (
      <Tooltip arrow title="More Pokemons">
        <Paper
          elevation={3}
          className={classes.linkStyle}
          onClick={() => handlePokemonLimitandOffset(nextUrl)}
        >
          <MoreRoundedIcon
            style={{
              transform: `rotate(180deg)`,
            }}
          />
        </Paper>
      </Tooltip>
    );
  }
}

export default withStyles(useStyles)(MorePokemon);
