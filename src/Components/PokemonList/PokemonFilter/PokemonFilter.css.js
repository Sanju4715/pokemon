export const useStyles = (theme) => ({
  root: {
    display: "flex",
  },
  inputRoot: {
    height: 70,
  },
  labelRoot: {
    fontSize: 18,
    fontWeight: "bold",
    backgroundColor: "#eee",
    paddingRight: 5,
    color: "#181818",
  },
  notchedOutline: {},
  labelFocused: { color: "black !important" },
  focused: {
    "& $notchedOutline": {
      borderColor: "black !important",
    },
  },
});
