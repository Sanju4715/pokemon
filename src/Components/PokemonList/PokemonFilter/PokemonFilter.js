import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

import { useStyles } from "./PokemonFilter.css";
import axios from "axios";

class PokemonFilter extends Component {
  constructor(props) {
    super(props);
    this.state = { habitats: [] };
  }

  componentDidMount() {
    this.getHabitats();
  }

  getHabitats = async () => {
    try {
      const response = await axios.get(
        `https://pokeapi.co/api/v2/pokemon-habitat`
      );
      this.setState({ habitats: response.data.results });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { classes, data, handleChange } = this.props;
    return (
      <div className={classes.root}>
        <TextField
          name="pokemonName"
          type="text"
          fullWidth
          margin="normal"
          variant="outlined"
          placeholder="Search By Pokemon Name"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              focused: classes.focused,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.labelRoot,
              focused: classes.labelFocused,
            },
            shrink: true,
          }}
          style={{ marginRight: 10 }}
          value={data.pokemonName}
          onChange={handleChange}
        />
        <TextField
          name="habitat"
          type="text"
          label="Search By Habitat"
          select
          fullWidth
          margin="normal"
          variant="outlined"
          SelectProps={{ native: true }}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              focused: classes.focused,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.labelRoot,
              focused: classes.labelFocused,
            },
            shrink: true,
          }}
          value={data.habitat}
          onChange={handleChange}
        >
          <option value="">Select Any One</option>
          {this.state.habitats.map((habitat, index) => (
            <option value={habitat.name} key={index}>
              {habitat.name.charAt(0).toUpperCase() + habitat.name.slice(1)}
            </option>
          ))}
        </TextField>
      </div>
    );
  }
}

export default withStyles(useStyles)(PokemonFilter);
