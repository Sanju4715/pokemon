export const useStyles = (theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
    },
  },
  content: {
    width: "30%",
    [theme.breakpoints.down("md")]: {
      width: "60%",
    },
  },
  linkStyle: {
    padding: 15,
    width: 15,
    height: 15,
    borderRadius: "50%",
    backgroundColor: "#181818",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#eee",
    "&:hover": {
      cursor: "pointer",
    },
  },
});
