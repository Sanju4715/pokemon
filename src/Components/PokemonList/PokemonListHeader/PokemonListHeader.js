import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";

import { useStyles } from "./PokemonListHeader.css";

class PokemonListHeader extends Component {
  render() {
    const { classes, pokemonName, habitat } = this.props;
    return (
      <div className={classes.root}>
        {pokemonName && (
          <ListItem className={classes.content}>
            <b style={{ flexGrow: 1 }}>Pokemon Name:</b>
            <div>{pokemonName}</div>
          </ListItem>
        )}
        {habitat && (
          <ListItem className={classes.content}>
            <b style={{ flexGrow: 1 }}>Habitat:</b>
            <div>{habitat}</div>
          </ListItem>
        )}
      </div>
    );
  }
}

export default withStyles(useStyles)(PokemonListHeader);
