import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import Grid from "@material-ui/core/Grid";

import { useStyles } from "./PokemonList.css";
import Header from "../Header/Header";
import PokemonFilter from "./PokemonFilter/PokemonFilter";
import { storePokemons } from "../Redux/Action/Action";
import PokemonCard from "./PokemonCard/PokemonCard";
import ScrollToTop from "../ScrollToTop/ScrollToTop";
import PokemonListHeader from "./PokemonListHeader/PokemonListHeader";
import MorePokemon from "./MorePokemon/MorePokemon";

class Pokemon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemons: [],
      isEmpty: false,
      searchBy: {
        pokemonName: "",
        habitat: "",
      },
      numbersOfPokemon: 100,
    };
  }

  componentDidMount() {
    if (this.props.pokemons.length === 0) {
      this.getPokemons(this.state.numbersOfPokemon, 0);
    }
  }

  getPokemons = async (numbersOfPokemon, pokemonOffset) => {
    try {
      const response = await axios.get(`https://pokeapi.co/api/v2/pokemon`, {
        params: {
          offset: pokemonOffset,
          limit: numbersOfPokemon,
        },
      });
      if (response.data.results.length !== 0) {
        let pokemons = [...this.props.pokemons, ...response.data.results];

        this.props.storePokemons(pokemons, response.data.next);
      }
    } catch (error) {
      console.log(error);
    }
  };

  handleChange = (e) => {
    this.setState({
      searchBy: { ...this.state.searchBy, [e.target.name]: e.target.value },
    });
  };

  handlePokemonLimitandOffset = (url) => {
    console.log("url", url);
    let tempArr = url.split(/(\d+)/);
    let offset = tempArr[3];
    let limit = tempArr[5];
    this.getPokemons(limit, offset);
  };

  render() {
    const { classes } = this.props;
    const { searchBy } = this.state;
    const { pokemons, nextUrl } = this.props;
    const pokemonFilter = pokemons.filter((data) => {
      return data.habitat
        ? data.name
            .toLowerCase()
            .indexOf(searchBy.pokemonName.toLowerCase()) !== -1 &&
            data.habitat
              .toLowerCase()
              .indexOf(searchBy.habitat.toLowerCase()) !== -1
        : data.name
            .toLowerCase()
            .indexOf(searchBy.pokemonName.toLowerCase()) !== -1;
    });
    return (
      <div className={classes.root}>
        <Header />
        <div className={classes.content}>
          <div className={classes.toolbar} />

          <div>
            <PokemonFilter
              data={this.state.searchBy}
              handleChange={this.handleChange}
            />

            <PokemonListHeader
              pokemonName={this.state.searchBy.pokemonName}
              habitat={this.state.searchBy.habitat}
              pokemonCount={pokemonFilter.length}
            />

            <MorePokemon
              nextUrl={nextUrl}
              handlePokemonLimitandOffset={this.handlePokemonLimitandOffset}
            />

            <div style={{ paddingLeft: 20, paddingTop: 20, paddingBottom: 20 }}>
              <Grid container spacing={3} style={{ width: "100%" }}>
                {pokemonFilter.map((pokemon, index) => (
                  <PokemonCard key={index} pokemon={pokemon} />
                ))}
              </Grid>
            </div>

            <MorePokemon
              nextUrl={nextUrl}
              handlePokemonLimitandOffset={this.handlePokemonLimitandOffset}
            />
            <ScrollToTop />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pokemons: state.PokemonReducer.pokemons,
    nextUrl: state.PokemonReducer.nextUrl,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    storePokemons: (pokemons, nextUrl) => {
      dispatch(storePokemons(pokemons, nextUrl));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(Pokemon));
