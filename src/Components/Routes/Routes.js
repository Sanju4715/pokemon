import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import About from "../About/About";
import PageNotFound from "../PageNotFound/PageNotFound";
import PokemonList from "../PokemonList/PokemonList";
import PokemonDetails from "../PokemonDetails/PokemonDetails";

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact component={PokemonList} />
        <Route path="/pokemon/:id" exact component={PokemonDetails} />
        <Route path="/about" exact component={About} />
        <Route path="/*" exact component={PageNotFound} />
      </Switch>
    );
  }
}

export default Routes;
