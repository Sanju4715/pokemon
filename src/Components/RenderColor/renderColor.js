export const renderColor = (pokemonType) => {
  switch (pokemonType) {
    case "normal":
      return "#a9a878";
    case "fighting":
      return "#c03028";
    case "flying":
      return "#a890f0";
    case "poison":
      return "#923a91";
    case "ground":
      return "#e0c068";
    case "rock":
      return "#b7a038";
    case "bug":
      return "#a8b921";
    case "ghost":
      return "#6667ba";
    case "steel":
      return "#b8b8d0";
    case "fire":
      return "#f07f2f";
    case "water":
      return "#6890f0";
    case "grass":
      return "#68c23e";
    case "electric":
      return "#f8d030";
    case "psychic":
      return "#f85888";
    case "ice":
      return "#98d8d8";
    case "dragon":
      return "#7038f9";
    case "dark":
      return "#6f5848";
    case "fairy":
      return "#ee99ad";
    case "unknown":
      return "#181818";
    case "shadow":
      return "#464646";
    default:
      return "transparent";
  }
};
