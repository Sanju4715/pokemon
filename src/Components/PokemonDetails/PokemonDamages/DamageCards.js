import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";

import { useStyles } from "./PokemonDamages.css";
import { renderColor } from "../../RenderColor/renderColor";

class DamageCards extends Component {
  render() {
    const { classes, array1, array2 } = this.props;
    return (
      <div>
        <Grid container spacing={3}>
          <Grid
            item
            xs={12}
            sm={12}
            md={6}
            xl={6}
            lg={6}
            className={classes.bodyContent}
          >
            <h3 style={{ float: "left" }}>Double Damage</h3>
            {array1.map((doubleDamageTo, index) => (
              <ListItem
                key={index}
                className={classes.content}
                style={{
                  background: renderColor(doubleDamageTo.name),
                }}
              >
                {doubleDamageTo.name.charAt(0).toUpperCase() +
                  doubleDamageTo.name.slice(1)}
              </ListItem>
            ))}
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            md={6}
            xl={6}
            lg={6}
            className={classes.bodyContent}
          >
            <h3 style={{ float: "left" }}>Half Damage</h3>
            {array2.map((halfDamageTo, index) => (
              <ListItem
                key={index}
                className={classes.content}
                style={{
                  background: renderColor(halfDamageTo.name),
                }}
              >
                {halfDamageTo.name.charAt(0).toUpperCase() +
                  halfDamageTo.name.slice(1)}
              </ListItem>
            ))}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(useStyles)(DamageCards);
