export const useStyles = (theme) => ({
  paper: { padding: 20 },
  typeStyle: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    marginRight: 5,
    color: "#fff",
    fontWeight: "bold",
  },
  rootContent: {
    display: "flex",
    textAlign: "center",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
    },
  },
  bodyContent: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      justifyContent: "center",
    },
  },
  content: {
    width: "30%",
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    color: "#fff",
    [theme.breakpoints.down("md")]: {
      width: "60%",
    },
  },
});
