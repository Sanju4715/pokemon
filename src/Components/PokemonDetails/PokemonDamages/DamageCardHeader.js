import React, { Component } from "react";
import { renderColor } from "../../RenderColor/renderColor";

class DamageCardHeader extends Component {
  render() {
    const { type } = this.props;
    return (
      <h2
        style={{
          color: "#eee",
          background: renderColor(type),
        }}
      >
        {type.charAt(0).toUpperCase() + type.slice(1)}
      </h2>
    );
  }
}

export default DamageCardHeader;
