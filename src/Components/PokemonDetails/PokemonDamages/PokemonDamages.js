import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

import { useStyles } from "./PokemonDamages.css";
import DamageCards from "./DamageCards";
import DamageCardHeader from "./DamageCardHeader";
import PageTitle from "../PageTitle/PageTitle";
import CustomLoader from "../../CustomLoader/CustomLoader";

class PokemonDamages extends Component {
  render() {
    const { classes, pokemonDetails } = this.props;
    const { damages, color } = pokemonDetails;
    return (
      <Grid item xs={12} sm={12} md={12} xl={12} lg={12}>
        <Paper elevation={5} className={classes.paper}>
          {damages ? (
            <div>
              <PageTitle title="Damages" color={color && color} />
              <div
                style={{
                  display: "flex",
                  textAlign: "center",
                }}
              >
                {damages &&
                  damages.map((damage, index) => (
                    <div
                      style={{
                        width: damages.length <= 1 ? "100%" : "50%",
                      }}
                      key={index}
                    >
                      <DamageCardHeader type={damage.type} />
                      <h3>Damaged To</h3>

                      <DamageCards
                        array1={damage.damageRelation.double_damage_to}
                        array2={damage.damageRelation.half_damage_to}
                      />
                    </div>
                  ))}
              </div>
              <br />
              <div style={{ display: "flex" }}>
                {damages &&
                  damages.map((damage, index) => (
                    <div
                      style={{
                        width: damages.length <= 1 ? "100%" : "50%",
                        textAlign: "center",
                      }}
                      key={index}
                    >
                      <DamageCardHeader type={damage.type} />
                      <h3>Damaged From</h3>
                      <DamageCards
                        array1={damage.damageRelation.double_damage_from}
                        array2={damage.damageRelation.half_damage_from}
                      />
                    </div>
                  ))}
              </div>
            </div>
          ) : (
            <CustomLoader />
          )}
        </Paper>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pokemonDetails: state.PokemonReducer.pokemonDetails,
  };
};

export default connect(
  mapStateToProps,
  null
)(withStyles(useStyles)(PokemonDamages));
