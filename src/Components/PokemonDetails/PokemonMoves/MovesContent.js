import React, { Component } from "react";
import ListItem from "@material-ui/core/ListItem";
import Paper from "@material-ui/core/Paper";

import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { renderColor } from "../../RenderColor/renderColor";

class MovesContent extends Component {
  constructor(props) {
    super(props);
    this.state = { detailStatus: false };
  }

  handleDetailStatus = (detailStatus) => {
    this.setState({ detailStatus });
  };

  render() {
    const { data } = this.props;
    const { detailStatus } = this.state;
    return (
      <div>
        <ListItem>
          <div style={{ flexGrow: 1 }}>
            <b
              style={{
                fontSize: 20,
              }}
            >
              {data.name.charAt(0).toUpperCase() + data.name.slice(1)}
            </b>
          </div>
          <div style={{ display: "flex" }}>
            <div
              style={{
                padding: 5,
                background: renderColor(data.type.name),
                flexGrow: 1,
              }}
            >
              {data.type.name.charAt(0).toUpperCase() + data.type.name.slice(1)}
            </div>
            {detailStatus ? (
              <ExpandLessIcon onClick={() => this.handleDetailStatus(false)} />
            ) : (
              <ExpandMoreIcon onClick={() => this.handleDetailStatus(true)} />
            )}
          </div>
        </ListItem>
        {detailStatus && (
          <Paper elevation={1} style={{ padding: 10, backgroundColor: "#eee" }}>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
              <div>
                <b>Power:</b>
                {data.power ? data.power : "N/A"}
              </div>
              <div>
                <b>Accuracy:</b>
                {data.accuracy ? `${data.accuracy}%` : "N/A"}
              </div>
              <div>
                <b>PP:</b>
                {data.pp ? data.pp : "N/A"}
              </div>
            </div>
            <br />
            <div style={{ textAlign: "center" }}>
              {data.effect_entries[0].effect}
            </div>
          </Paper>
        )}
      </div>
    );
  }
}

export default MovesContent;
