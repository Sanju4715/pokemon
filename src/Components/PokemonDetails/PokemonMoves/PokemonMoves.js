import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

import { useStyles } from "./PokemonMoves.css";
import PageTitle from "../PageTitle/PageTitle";
import PokemonMovesDetails from "./PokemonMovesDetails/PokemonMovesDetails";
import CustomLoader from "../../CustomLoader/CustomLoader";
import { storeMoveDetails } from "../../Redux/Action/Action";

class PokemonMoves extends Component {
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.moveDetails && nextProps.moveDetails.length === 0) {
      this.getMoveDetails(
        nextProps.pokemonDetails.moves && nextProps.pokemonDetails.moves
      );
    }
  }

  getMoveDetails = async (moves) => {
    try {
      let tempMoveDetails = [];
      for (let i = 0; i < moves.length; i++) {
        let moveDetails = await axios.get(moves[i].url);
        tempMoveDetails.push({
          id: moveDetails.data.id,
          name: moveDetails.data.name,
          accuracy: moveDetails.data.accuracy,
          power: moveDetails.data.power,
          pp: moveDetails.data.pp,
          priority: moveDetails.data.priority,
          contest_type: moveDetails.data.contest_type,
          damage_class: moveDetails.data.damage_class,
          type: moveDetails.data.type,
          target: moveDetails.data.target,
          effect_entries: moveDetails.data.effect_entries,
        });
      }
      this.props.storeMoveDetails(tempMoveDetails);
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { classes, pokemonDetails } = this.props;
    const { color } = pokemonDetails;
    const { moveDetails } = this.props;
    const statusDamageClassFilter =
      moveDetails.length !== 0 &&
      moveDetails.filter((data) => data.damage_class.name === "status");
    const physicalDamageClassFilter =
      moveDetails.length !== 0 &&
      moveDetails.filter((data) => data.damage_class.name === "physical");
    const specialDamageClassFilter =
      moveDetails.length !== 0 &&
      moveDetails.filter((data) => data.damage_class.name === "special");
    return (
      <Grid item xs={12} sm={12} md={12} xl={12} lg={12}>
        <Paper elevation={5} className={classes.paper}>
          <PageTitle title="Moves" color={color} />
          <br />
          {moveDetails.length !== 0 ? (
            <div className={classes.rootContent}>
              <div className={classes.content}>
                <PokemonMovesDetails
                  damageClass="Special"
                  moveDetails={specialDamageClassFilter}
                  color={color}
                />
              </div>
              <div className={classes.content}>
                <PokemonMovesDetails
                  damageClass="Physical"
                  moveDetails={physicalDamageClassFilter}
                  color={color}
                />
              </div>
              <div className={classes.content}>
                <PokemonMovesDetails
                  damageClass="Status"
                  moveDetails={statusDamageClassFilter}
                  color={color}
                />
              </div>
            </div>
          ) : (
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CustomLoader />
            </div>
          )}
        </Paper>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pokemonDetails: state.PokemonReducer.pokemonDetails,
    moveDetails: state.PokemonReducer.moveDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    storeMoveDetails: (moveDetails) => {
      dispatch(storeMoveDetails(moveDetails));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(PokemonMoves));
