import React, { Component } from "react";
import MovesContent from "../MovesContent";

class PokemonMovesDetails extends Component {
  render() {
    const { damageClass, moveDetails, color } = this.props;
    return (
      moveDetails &&
      moveDetails.length !== 0 && (
        <div>
          <div
            style={{
              color: "#eee",
              background: color,
              fontSize: 20,
              borderRadius: 20,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              fontWeight: "bold",
            }}
          >
            {damageClass} ( {moveDetails.length} )
          </div>
          {moveDetails.map((move, index) => (
            <MovesContent key={index} index={index} data={move} />
          ))}
        </div>
      )
    );
  }
}

export default PokemonMovesDetails;
