export const useStyles = (theme) => ({
  paper: { padding: 20 },
  rootContent: {
    display: "flex",
    [theme.breakpoints.down("sm")]: { flexDirection: "column" },
  },
  content: {
    width: "33%",
    marginRight: 10,
    [theme.breakpoints.down("sm")]: { width: "100%", marginBottom: 10 },
  },
});
