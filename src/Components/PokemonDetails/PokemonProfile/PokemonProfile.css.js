export const useStyles = (theme) => ({
  paper: { padding: 20 },
  pokeball: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 35,
    height: 35,
    marginRight: 10,
    borderRadius: "50%",
    "&:hover": {
      cursor: "pointer",
      "&#red": {
        boxShadow: `0 0 0 3px rgba(221,45,81)`,
      },
      "&#blue": {
        boxShadow: `0 0 0 3px rgba(0,114,177)`,
      },
    },
  },
  rootContent: {
    display: "flex",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
    },
  },
  content: {
    width: "50%",
    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
  },
});
