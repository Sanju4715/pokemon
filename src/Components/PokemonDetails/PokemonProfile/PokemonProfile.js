import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import ListItem from "@material-ui/core/ListItem";

import { useStyles } from "./PokemonProfile.css";
import PageTitle from "../PageTitle/PageTitle";

class PokemonProfile extends Component {
  render() {
    const { classes, pokemonDetails } = this.props;
    const {
      color,
      height,
      weight,
      shape,
      captureRate,
      growthRate,
      eggGroups,
      habitat,
      hatchCounter,
      abilities,
    } = pokemonDetails;
    return (
      <Grid item xs={12} sm={12} md={6} xl={6} lg={6}>
        <Paper elevation={5} className={classes.paper}>
          <PageTitle title="Profile" color={color} />

          <div className={classes.rootContent}>
            <div className={classes.content}>
              <ListItem>
                <b style={{ flexGrow: 1 }}>Height</b>
                <div>{height && height} m</div>
              </ListItem>
              <ListItem>
                <b style={{ flexGrow: 1 }}>Catch Rate</b>
                <div>{captureRate && captureRate}%</div>
              </ListItem>

              <ListItem>
                <b style={{ flexGrow: 1 }}>Growth Rate</b>
                <div>
                  {growthRate &&
                    growthRate.charAt(0).toUpperCase() + growthRate.slice(1)}
                </div>
              </ListItem>
              <ListItem>
                <b style={{ flexGrow: 1 }}>Egg Groups</b>
                {eggGroups &&
                  eggGroups.map((egg, index) => (
                    <div key={index}>
                      [{egg.charAt(0).toUpperCase() + egg.slice(1)}
                      ]&nbsp;
                    </div>
                  ))}
              </ListItem>
            </div>
            <div className={classes.content}>
              <ListItem>
                <b style={{ flexGrow: 1 }}>Weight</b>
                <div>{weight && weight} kg</div>
              </ListItem>

              <ListItem>
                <b style={{ flexGrow: 1 }}>Shape</b>
                <div>
                  {shape && shape.charAt(0).toUpperCase() + shape.slice(1)}
                </div>
              </ListItem>
              <ListItem>
                <b style={{ flexGrow: 1 }}>Habitat</b>
                <div>
                  {habitat &&
                    habitat.charAt(0).toUpperCase() + habitat.slice(1)}
                </div>
              </ListItem>
              <ListItem>
                <b style={{ flexGrow: 1 }}>Hatch Counter</b>
                <div>{hatchCounter && hatchCounter}</div>
              </ListItem>
            </div>
          </div>

          <ListItem>
            <b style={{ flexGrow: 1 }}>Abilities</b>
            {abilities &&
              abilities.map((ability, index) => (
                <div key={index}>
                  [{ability.charAt(0).toUpperCase() + ability.slice(1)}
                  ]&nbsp;
                </div>
              ))}
          </ListItem>
        </Paper>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pokemonDetails: state.PokemonReducer.pokemonDetails,
  };
};

export default connect(
  mapStateToProps,
  null
)(withStyles(useStyles)(PokemonProfile));
