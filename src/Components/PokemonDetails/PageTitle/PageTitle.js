import React, { Component } from "react";
import Divider from "@material-ui/core/Divider";

class PageTitle extends Component {
  render() {
    const { title, id, color } = this.props;
    return (
      <>
        <b
          style={{
            width: "100%",
            display: "flex",
            fontSize: 25,
            color: color,
          }}
        >
          <b style={{ flexGrow: 1 }}>{title}</b>
          {id && <div>#{id}</div>}
        </b>
        <Divider />
      </>
    );
  }
}

export default PageTitle;
