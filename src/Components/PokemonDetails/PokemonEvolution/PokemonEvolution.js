import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

import { useStyles } from "./PokemonEvolution.css";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import CustomLoader from "../../CustomLoader/CustomLoader";
import PageTitle from "../PageTitle/PageTitle";

class PokemonEvolution extends Component {
  renderImage = (url) => {
    let tempArr = url && url.split("/");
    let pokemonId = tempArr && tempArr[tempArr.length - 2];
    return (
      pokemonId && (
        <img
          src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemonId}.svg`}
          alt={pokemonId}
          style={{ width: 75, height: 75 }}
        />
      )
    );
  };

  renderEvolution = (
    species,
    evolutionDetails,
    evolvesTo,
    color,
    deviceType
  ) => {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: deviceType === "desktop" ? "row" : "column",
          color: color,
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: deviceType === "desktop" ? "row" : "column",
            alignItems: "center",
            textAlign: "center",
          }}
        >
          <div>
            <div>{this.renderImage(species && species.url)}</div>
            <b style={{ fontSize: 18 }}>
              {species &&
                species.name.charAt(0).toUpperCase() + species.name.slice(1)}
            </b>
            {evolutionDetails ? (
              evolutionDetails.length !== 0 ? (
                <div>
                  Evolves into{" "}
                  <b>
                    {species &&
                      species.name.charAt(0).toUpperCase() +
                        species.name.slice(1)}
                  </b>{" "}
                  at level {evolutionDetails[0].min_level}
                </div>
              ) : (
                <div>
                  Evolves from{" "}
                  <b>
                    {species &&
                      species.name.charAt(0).toUpperCase() +
                        species.name.slice(1)}
                  </b>
                </div>
              )
            ) : (
              <CustomLoader />
            )}
          </div>
          {evolvesTo && evolvesTo.length !== 0 && (
            <div style={{ alignItems: "center", justifyContent: "center" }}>
              {deviceType === "desktop" ? (
                <NavigateNextIcon style={{ width: 50, height: 50 }} />
              ) : (
                <ExpandMoreIcon style={{ width: 50, height: 50 }} />
              )}
            </div>
          )}
        </div>
        {evolvesTo &&
          evolvesTo.length !== 0 &&
          this.renderEvolution(
            evolvesTo[0].species,
            evolvesTo[0].evolution_details,
            evolvesTo[0].evolves_to,
            color,
            deviceType
          )}
      </div>
    );
  };

  render() {
    const { classes, pokemonDetails } = this.props;
    const { evolutions, color } = pokemonDetails;
    return (
      <Grid item xs={12} sm={12} md={6} xl={6} lg={6}>
        <Paper elevation={5} className={classes.paper}>
          <PageTitle title="Evolution" color={color} />
          <div className={classes.sectionDesktop}>
            {evolutions &&
              this.renderEvolution(
                evolutions.species,
                evolutions.evolution_details,
                evolutions.evolves_to,
                color,
                "desktop"
              )}
          </div>
          <div className={classes.sectionMobile}>
            {evolutions &&
              this.renderEvolution(
                evolutions.species,
                evolutions.evolution_details,
                evolutions.evolves_to,
                color,
                "mobile"
              )}
          </div>
        </Paper>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pokemonDetails: state.PokemonReducer.pokemonDetails,
  };
};

export default connect(
  mapStateToProps,
  null
)(withStyles(useStyles)(PokemonEvolution));
