import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";

import { useStyles } from "./PokeballVersions.css";
import RedPokeball from "../../Assets/RedPokeball.svg";
import BluePokeball from "../../Assets/BluePokeball.svg";
import YellowPokeball from "../../Assets/YellowPokeball.svg";
import RubyPokeball from "../../Assets/RubyPokeball.svg";
import EmeraldPokeball from "../../Assets/EmeraldPokeball.svg";
import SapphirePokeball from "../../Assets/SapphirePokeball.svg";

class PokeballVersions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokeballVersions: [
        {
          name: "red",
          hexValue: "#dd2d51",
          icon: (
            <img
              src={RedPokeball}
              alt="RedPokeball"
              style={{
                width: 40,
                height: 40,
              }}
            />
          ),
        },
        {
          name: "blue",
          hexValue: "#6890f0",
          icon: (
            <img
              src={BluePokeball}
              alt="BluePokeball"
              style={{
                width: 40,
                height: 40,
              }}
            />
          ),
        },
        {
          name: "yellow",
          hexValue: "#f8d030",
          icon: (
            <img
              src={YellowPokeball}
              alt="YellowPokeball"
              style={{
                width: 40,
                height: 40,
              }}
            />
          ),
        },
        {
          name: "ruby",
          hexValue: "#8c1a24",
          icon: (
            <img
              src={RubyPokeball}
              alt="RubyPokeball"
              style={{
                width: 40,
                height: 40,
              }}
            />
          ),
        },
        {
          name: "emerald",
          hexValue: "#5cc17d",
          icon: (
            <img
              src={EmeraldPokeball}
              alt="EmeraldPokeball"
              style={{
                width: 40,
                height: 40,
              }}
            />
          ),
        },
        {
          name: "sapphire",
          hexValue: "#0f52ba",
          icon: (
            <img
              src={SapphirePokeball}
              alt="SapphirePokeball"
              style={{
                width: 40,
                height: 40,
              }}
            />
          ),
        },
      ],
      version: "red",
      hexValue: "#dd2d51",
    };
  }

  handleVersion = (version, hexValue) => {
    this.setState({ version, hexValue });
  };

  renderText = (version) => {
    let tempArr = this.props.versionText.find(
      (data) => data.version.name === version
    );
    return tempArr && tempArr.flavor_text;
  };

  render() {
    const { classes, color } = this.props;
    const { version, hexValue, pokeballVersions } = this.state;
    return (
      <div style={{ width: "100%", padding: 5 }}>
        <b style={{ color: color, fontSize: 16, marginRight: 10 }}>Versions:</b>
        <div style={{ display: "flex", alignItems: "center", paddingTop: 5 }}>
          {pokeballVersions.map((pokeball, index) => (
            <div
              key={index}
              className={classes.pokeball}
              id={pokeball.name}
              onClick={() =>
                this.handleVersion(pokeball.name, pokeball.hexValue)
              }
              style={{
                boxShadow:
                  version === pokeball.name && `0 0 0 3px ${pokeball.hexValue}`,
              }}
            >
              <Tooltip arrow title={pokeball.name}>
                {pokeball.icon}
              </Tooltip>
            </div>
          ))}
        </div>
        <p style={{ textAlign: "center", color: hexValue, fontSize: 16 }}>
          {this.renderText(version)}
        </p>
      </div>
    );
  }
}

export default withStyles(useStyles)(PokeballVersions);
