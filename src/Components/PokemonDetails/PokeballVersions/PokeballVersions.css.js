export const useStyles = (theme) => ({
  pokeball: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 35,
    height: 35,
    marginRight: 10,
    borderRadius: "50%",
    "&:hover": {
      cursor: "pointer",
      "&#red": {
        boxShadow: `0 0 0 3px #dd2d51`,
      },
      "&#blue": {
        boxShadow: `0 0 0 3px #6890f0`,
      },
      "&#yellow": {
        boxShadow: `0 0 0 3px #f8d030`,
      },
      "&#ruby": {
        boxShadow: `0 0 0 3px #8c1a24`,
      },
      "&#emerald": {
        boxShadow: `0 0 0 3px #5cc17d`,
      },
      "&#sapphire": {
        boxShadow: `0 0 0 3px #0f52ba`,
      },
    },
  },
});
