import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";

import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import { useStyles } from "./PokemonDetails.css";
import PokemonMoves from "./PokemonMoves/PokemonMoves";
import PokemonEvolution from "./PokemonEvolution/PokemonEvolution";
import { renderColor } from "../RenderColor/renderColor";
import PokemonStats from "./PokemonStats/PokemonStats";
import PokemonProfile from "./PokemonProfile/PokemonProfile";
import PokemonDamages from "./PokemonDamages/PokemonDamages";
import ScrollToTop from "../ScrollToTop/ScrollToTop";
import { storePokemonDetails } from "../Redux/Action/Action";

class PokemonDetails extends Component {
  componentDidMount() {
    if (this.props.match.params.id) {
      if (this.props.match.params.id !== this.props.pokemonDetails.pokemonId) {
        this.getDetails(this.props.match.params.id);
      }
    }
  }

  getDetails = async (pokemonId) => {
    try {
      const response = await axios.get(
        `https://pokeapi.co/api/v2/pokemon/${pokemonId}`
      );
      let damageRelation = [];
      for (let i = 0; i < response.data.types.length; i++) {
        let damageResponse = await axios.get(response.data.types[i].type.url);
        damageRelation.push({
          type: damageResponse.data.name,
          damageRelation: damageResponse.data.damage_relations,
        });
      }
      const speciesResponse = await axios.get(response.data.species.url);
      const evolutionResponse = await axios.get(
        speciesResponse.data.evolution_chain.url
      );

      let tempData = {
        pokemonId: pokemonId,
        pokemonName: response.data.name,
        pokemonImage: response.data.sprites.other.dream_world.front_default,
        color: speciesResponse.data.color.name,
        abilities: response.data.abilities.map((data) => data.ability.name),
        types: response.data.types.map((data) => data.type.name),
        height: response.data.height,
        weight: response.data.weight,
        shape: speciesResponse.data.shape.name,
        captureRate: speciesResponse.data.capture_rate,
        growthRate: speciesResponse.data.growth_rate.name,
        versionText: speciesResponse.data.flavor_text_entries,
        moves: response.data.moves.map((move) => move.move),
        stats: response.data.stats,
        species: speciesResponse.data,
        eggGroups: speciesResponse.data.egg_groups.map((data) => data.name),
        habitat: speciesResponse.data.habitat.name,
        hatchCounter: speciesResponse.data.hatch_counter,
        generation: speciesResponse.data.generation.name,
        damages: damageRelation,
        evolutions: evolutionResponse.data.chain,
      };
      this.props.storePokemonDetails(tempData);
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { classes } = this.props;
    const { types } = this.props.pokemonDetails;
    return (
      <div
        className={classes.root}
        style={{
          background:
            types && types.length < 2
              ? renderColor(types[0])
              : `linear-gradient(to right, ${renderColor(
                  types && types[0]
                )},${renderColor(types && types[1])})`,
        }}
      >
        <Link
          href="/"
          color="inherit"
          style={{
            display: "flex",
            color: "#181818",
            fontSize: 20,
            fontWeight: "bold",
            alignItems: "center",
          }}
        >
          <ArrowBackIcon />
          Go Back
        </Link>
        <br />
        <Grid container spacing={3}>
          <PokemonStats />

          <PokemonProfile />

          <PokemonEvolution />

          <PokemonDamages />

          <PokemonMoves />
        </Grid>
        <ScrollToTop />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pokemonDetails: state.PokemonReducer.pokemonDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    storePokemonDetails: (pokemonDetails) => {
      dispatch(storePokemonDetails(pokemonDetails));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(PokemonDetails));
