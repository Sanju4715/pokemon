import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";

import { useStyles } from "./CustomProgressBar.css";

class CustomProgressBar extends Component {
  render() {
    const { classes, color, value } = this.props;
    let percentage = parseInt(((value / (value + 50)) * 100).toFixed(0));
    let remaining = 100 - percentage;
    return (
      <div className={classes.root}>
        <div
          style={{
            width: "98%",
            display: "flex",
            height: 30,
            marginRight: 5,
          }}
        >
          <div
            style={{
              width: `${percentage}%`,
              backgroundColor: color,
              borderTopLeftRadius: 20,
              borderBottomLeftRadius: 20,
            }}
          />
          <div
            style={{
              width: `${remaining}%`,
              backgroundColor: "#eee",
              borderTopRightRadius: 20,
              borderBottomRightRadius: 20,
            }}
          />
        </div>
        <b>{value}</b>
      </div>
    );
  }
}

export default withStyles(useStyles)(CustomProgressBar);
