export const useStyles = (theme) => ({
  paper: { padding: 20 },
  typeStyle: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    marginRight: 5,
    color: "#fff",
    fontWeight: "bold",
  },
  rootContent: {
    display: "flex",
    textAlign: "center",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
    },
  },
});
