import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import Skeleton from "@material-ui/lab/Skeleton";

import { useStyles } from "./PokemonStats.css";
import { renderColor } from "../../RenderColor/renderColor";
import PageTitle from "../PageTitle/PageTitle";
import PokeballVersions from "../PokeballVersions/PokeballVersions";
import CustomProgressBar from "./CustomProgressBar/CustomProgressBar";

class PokemonStats extends Component {
  render() {
    const { classes, pokemonDetails } = this.props;
    const {
      pokemonId,
      pokemonName,
      types,
      color,
      pokemonImage,
      versionText,
      stats,
    } = pokemonDetails;
    return (
      <Grid item xs={12} sm={12} md={12} xl={12} lg={12}>
        <Paper elevation={5} className={classes.paper}>
          <PageTitle
            title={
              pokemonName &&
              pokemonName.charAt(0).toUpperCase() + pokemonName.slice(1)
            }
            color={color && color}
            id={pokemonId && pokemonId}
          />
          <Divider />

          <br />

          <div style={{ display: "flex" }}>
            {types &&
              types.map((type, index) => (
                <div
                  key={index}
                  className={classes.typeStyle}
                  style={{
                    background: renderColor(type),
                  }}
                >
                  {type.charAt(0).toUpperCase() + type.slice(1)} &nbsp;
                </div>
              ))}
          </div>
          <div className={classes.rootContent}>
            <div className={classes.content1}>
              {pokemonImage && pokemonImage ? (
                <img
                  src={pokemonImage}
                  alt={pokemonName}
                  style={{ width: 250, height: 250 }}
                />
              ) : (
                <Skeleton variant="rect" width={210} height={118} />
              )}
            </div>

            <div style={{ width: "100%", textAlign: "left" }}>
              {stats && stats.length !== 0 ? (
                stats.map((stat, index) => (
                  <div key={index}>
                    <b style={{ padding: 5 }}>
                      {stat.stat.name.charAt(0).toUpperCase() +
                        stat.stat.name.slice(1)}{" "}
                      - {stat.base_stat}
                    </b>

                    <CustomProgressBar value={stat.base_stat} color={color} />
                    <br />
                  </div>
                ))
              ) : (
                <Skeleton
                  variant="rect"
                  style={{ marginLeft: 30, width: "100%" }}
                />
              )}
            </div>
          </div>
          {versionText && (
            <PokeballVersions color={color} versionText={versionText} />
          )}
        </Paper>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pokemonDetails: state.PokemonReducer.pokemonDetails,
  };
};

export default connect(
  mapStateToProps,
  null
)(withStyles(useStyles)(PokemonStats));
