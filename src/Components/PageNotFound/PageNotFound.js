import React, { Component } from "react";
import Link from "@material-ui/core/Link";

class PageNotFound extends Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
          fontSize: 50,
        }}
      >
        <div>Page Not Found</div>
        <Link href="/" color="inherit">
          Go To Home
        </Link>
      </div>
    );
  }
}

export default PageNotFound;
