import React, { Component } from "react";
import Pokeball from "../Assets/pokeball.gif";

class CustomLoader extends Component {
  render() {
    return (
      <img src={Pokeball} alt="loading" style={{ width: 100, height: 100 }} />
    );
  }
}

export default CustomLoader;
