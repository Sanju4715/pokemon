import {
  POKEMON_DETAILS,
  POKEMON_MOVE_DETAILS,
  STORE_POKEMONS,
  UPDATE_POKEMONS,
} from "../Types/Types";

const initState = {
  pokemons: [],
  nextUrl: "",
  pokemonDetails: {},
  moveDetails: [],
};

const PokemonReducer = (state = initState, action) => {
  switch (action.type) {
    case STORE_POKEMONS:
      return {
        ...state,
        pokemons: action.payload,
        nextUrl: action.nextUrl,
        pokemonDetails: {},
      };

    case UPDATE_POKEMONS:
      const pokemonIndex = state.pokemons.findIndex(
        (data) => data.name === action.payload.name
      );
      state.pokemons.splice(pokemonIndex, 1, action.payload);
      let newArr = state.pokemons;
      return { ...state, pokemons: newArr };

    case POKEMON_DETAILS:
      return { ...state, pokemonDetails: action.payload, moveDetails: [] };

    case POKEMON_MOVE_DETAILS:
      return { ...state, moveDetails: action.payload };

    default:
      return state;
  }
};

export default PokemonReducer;
