import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import Reducer from "../Reducer/ReducerIndex";

const persistConfig = {
  key: "root",
  storage,
};

const persistorReducer = persistReducer(persistConfig, Reducer);
export const store = createStore(persistorReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);

export default (store, persistor);
