import {
  POKEMON_DETAILS,
  POKEMON_MOVE_DETAILS,
  STORE_POKEMONS,
  UPDATE_POKEMONS,
} from "../Types/Types";

export const storePokemons = (pokemons, nextUrl) => (dispatch) => {
  dispatch({ type: STORE_POKEMONS, payload: pokemons, nextUrl });
};

export const updatePokemons = (pokemons) => (dispatch) => {
  dispatch({ type: UPDATE_POKEMONS, payload: pokemons });
};

export const storePokemonDetails = (pokemonDetails) => (dispatch) => {
  dispatch({ type: POKEMON_DETAILS, payload: pokemonDetails });
};

export const storeMoveDetails = (moveDetails) => (dispatch) => {
  dispatch({ type: POKEMON_MOVE_DETAILS, payload: moveDetails });
};
